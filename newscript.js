var languages = [" please select a language", "java", "springBoot", "HTML", "CSS", "JS"];
function addLanguages() {
    let languagePicker = document.getElementById("languagePicker");

    while (languagePicker.lastElementChild) {
        languagePicker.removeChild(languagePicker.lastElementChild);
    }
    languages.forEach((language) => {
        let optionElement = document.createElement('option');
        document.getElementById("languagePicker").appendChild(optionElement);
        optionElement.setAttribute("value", language);//diff b/n 11 n 12 lines
        optionElement.innerText = language;
    })
}
addLanguages();

function languageSelected() {
    let selectedLanguage = document.getElementById("languagePicker").value;
    languages = languages.filter(language => language != selectedLanguage)
    addLanguages();
    let languageLabel = document.createElement("label");
    languageLabel.innerText = selectedLanguage + "      ";
    document.getElementById("form").appendChild(languageLabel);
    languageLabel.addEventListener("click", function(){
        languages.push(languageLabel.innerText);
        addLanguages();
        document.getElementById("form").removeChild(languageLabel);
    })

}